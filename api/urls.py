from django.urls import path
from api.views import *

urlpatterns = [
    path(r'messages/', MessageView.as_view()),
    path(r'messages/<int:pk>', MessageView.as_view()),
    path(r'messages/<slug:mode>', MessageView.as_view())
]